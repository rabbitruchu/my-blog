import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)
export default new Vuex.Store({
    state: {
        email: "",
        openid: "",
        name: "",
        img: ""
    },
    mutations: {
        setEmail(state, email) {
            state.email = email
        },
        setOpenid(state, openid) {
            state.openid = openid
        },
        setName(state, name) {
            state.name = name
        },
        setImg(state, img) {
            state.img = img
        }
    },
    actions: {

    }
})