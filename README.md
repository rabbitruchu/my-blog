#  **从前有个如初**      
[从前有个如初](http://www.supert.net.cn)个人网站布局开源（不带后台程序）

> 由于近期身体原因     不在打算更新维护网站了。   
 
> emmmm....         所以打算开源网站的设计布局

> emmmm....         有喜欢布局的大佬可以将项目clone下来然后根据自己的喜好修改

> 该项目是用基于vue的nuxt框架开发的，主要是为了解决浏览器的SEO问题

> 啰嗦一句：从一开始设计网站的时候就打算用企鹅账号作为登录功能

> 如果有大佬打算将项目重构为.html文件的项目，只需要知道每个组件里面的css样式基本都是独立的就行了。

> 至于通用的css样式，我将它们存放到 > assets/css/common.css 里面了


> 最后，麻烦各位大佬给个   Star     :sleepy: 
:sleepy: 
:sleepy: 
:sleepy: 
:sleepy: 
:sleepy: 

### Star


### 企鹅账号登录功能大致思路

```

一、你需要前往QQ互联的官网申请权限。在这一步中主要是为了获取一个 APPID 和一个 APPURL（也就是回调地址。回调地址需要自行设置）。

二、在你项目的页面中添加一个按钮。主要作为跳转到 TX 的登录界面。

三、如果是在 Vue 项目中，建议大佬你在触发按钮后和跳转登录界面前，记录一下当前的 URL 地址（后续会用到。建议存Cookie）

四、在跳转页面选择了登录账号后，页面会再次进行跳转。此时跳转的 URL 地址是第一步中所提到的 APPURL ，并且返回了一个 Code 给你。

五、当你拿到 Code 后，可以向后台服务器（你自己的后台程序）发送一个请求，将 Code 传递过去。

六、此时来到你的后端程序中。通过前端传递的 Code 和 APPID、APPKEY、APPURL 向 TX 提供的API接口发送请求，主要是为了获取一个Token。（ APPID 和 APPKEY 在QQ互联中可以找到 ）

七、拿到 Token 后，需要再次向 TX 提供的第二个接口发送请求，主要为了拿到OpenId.

八、拿到 OpenId 后，需要再一次向 TX 提供的第三个接口发送请求。此时你就应该拿到了用户的一些信息，然后你需要筛选出前端需要的数据并返回  
  JSON。（需要加密的话，烦请自行操作）
九、此时再次来到你的前端代码中。拿到后端返回的 JSON 后，将需要保存的数据保存了，例如昵称、头像地址等。

十、拿到第二步中建议保存的 URL 地址，然后做一个页面的跳转。

回顾：
      1、在用户点击了登录按钮的时候做了两个操作，第一、保存了当前的 URL 地址；第二跳转到登录界面。
      2、选择完账后登录后，页面会跳转到回调域地址，然后拿到 Code 传递给后端程序。
      3、后端通过 Code、APPID、APPKEY、APPURL 拿到 Token ，然后通过 Token 拿到 OpenId，然后通过 OpenId 拿到用户信息。
      4、后端筛选出前端需要的数据后，做部分数据的加密处理，并返回给前端。
      5、前端拿到返回的数据后，保存需要的数据，然后通过获取之前保存的 URL 地址做一个跳转，此时登录完成后就会回到之前的 URL 地址。

```




### 效果图如下
![首页](https://images.gitee.com/uploads/images/2020/0619/194003_e4872be7_2116967.png "9B3DAE4371ABD73DF14AE9DF1090060C.png")
![技术类日志](https://images.gitee.com/uploads/images/2020/0619/194032_857bcf32_2116967.png "7A89B7965C0E6BE071B34D400723F156.png")
![资源分享 ---- 主要提供下载链接](https://images.gitee.com/uploads/images/2020/0619/194054_6713bd07_2116967.png "F7692D789D5A523021D8BAC8C3680EF0.png")
![生活日志](https://images.gitee.com/uploads/images/2020/0619/194147_a3334c83_2116967.png "E390BDA353A2FF0B3C28C86C42EE0E61.png")
![留言板](https://images.gitee.com/uploads/images/2020/0619/194210_1ea5f598_2116967.png "21B50AF0ACB597157F606FC2172A847A.png")
![友链](https://images.gitee.com/uploads/images/2020/0619/194229_ae73bd28_2116967.png "6795D7AF48CA6A894F1FB2566386A7D6.png")
![关于博主的一些简单的介绍](https://images.gitee.com/uploads/images/2020/0619/194247_d2641ef5_2116967.png "7EA3F02F1EED7A817235054F319FFF9D.png")
![用户反馈 ---- 用户匿名操作，不需要实现登录功能](https://images.gitee.com/uploads/images/2020/0619/194314_8f76e213_2116967.png "AA29C743501601B6567B89B70C0CF3F7.png")
![网站的更新记录](https://images.gitee.com/uploads/images/2020/0619/194350_bec55a73_2116967.png "9F5A745F9EA5341BB03BBF18E73D64CC.png")



# my-blog

> My striking Nuxt.js project

## Build Setup

```bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm run start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, check out [Nuxt.js docs](https://nuxtjs.org).



