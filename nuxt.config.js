export default {
    mode: 'universal',
    /*
     ** Headers of the page
     */
    head: {
        title: '从前有个如初。',
        meta: [
            { charset: 'utf-8' },
            { name: 'viewport', content: 'width=device-width, initial-scale=1' },
            { hid: 'description', name: 'description', content: process.env.npm_package_description || '' }
        ],
        link: [
            { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
            { rel: 'stylesheet', type: 'text/css', href: 'https://use.fontawesome.com/releases/v5.8.1/css/all.css' },
            { rel: 'stylesheet', type: 'text/css', href: 'https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/magnific-popup.min.css' }
        ],
        script: [
            { type: "text/javascript", charset: "utf-8", src: "https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js" }
        ]
    },
    /*
     ** Customize the progress-bar color
     */
    loading: { color: '#33cb98' },
    /*
     ** Global CSS
     */
    css: [
        '~/assets/css/common.css',
        '~/assets/css/iconfont.css',
        '~/assets/css/style.css'
    ],
    /*
     ** Plugins to load before mounting the App
     */
    plugins: [{
        src: '~/plugins/page',
        ssr: false //关闭ssr
    }],
    /*
     ** Nuxt.js dev-modules
     */
    buildModules: [],
    /*
     ** Nuxt.js modules
     */
    modules: [
        '@nuxtjs/axios'
    ],
    axios: {
        prefix: '/api/',
        proxy: true
    },
    proxy: {
        // 配置跨域
        '/api': {
            target: 'http://127.0.0.1:8081/api/',
            ws: true,
            changOrigin: true,
            pathRewrite: {
                '^/api': ''
            }
        }
    },
    /*
     ** Build configuration
     */
    build: {
        /*
         ** You can extend webpack config here
         */
        extend(config, ctx) {
            const sassResourcesLoader = {
                loader: 'sass-resources-loader',
                options: {
                    resources: ['assets/style/theme.less', 'assets/style/mixins.less']
                }
            }
            config.module.rules.forEach(rule => {
                if (rule.test.toString() === '/\\.vue$/') {
                    rule.options.loaders.less.push(sassResourcesLoader)
                }
                if (rule.test.toString() === '/\\.less$/') {
                    rule.use.push(sassResourcesLoader)
                }
            })
        }
    }
}